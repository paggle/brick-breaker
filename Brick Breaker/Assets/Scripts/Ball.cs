﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	public Rigidbody2D ball;
	public MeshRenderer startText;
	public SpriteRenderer ballRender;
	public CircleCollider2D ballCollider;
	public BoxCollider2D paddleCollider;
	public BoxCollider2D topWall;
	public BoxCollider2D rightWall;
	public BoxCollider2D leftWall;
	public BoxCollider2D botWall;
	public bool gameStart;
	// Use this for initialization
	void Start () {
		gameStart = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space) && !gameStart) {
			gameStart = true;
			startText.enabled = false;
			ballRender.enabled = true;
			int startXVel = Random.Range (-5, 6);
			Vector2 startForce = new Vector2(startXVel, -5);
			ball.velocity = startForce;
		}


	}
	void FixedUpdate() {
		if (ballCollider.IsTouching (paddleCollider) || ballCollider.IsTouching(topWall)) {
			ball.velocity = new Vector2 (ball.velocity.x, -ball.velocity.y);
		}
		if (ballCollider.IsTouching (leftWall) || ballCollider.IsTouching(rightWall)) {
			ball.velocity = new Vector2 (-ball.velocity.x, ball.velocity.y);
		}
	}
}
