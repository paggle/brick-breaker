﻿using UnityEngine;
using System.Collections;

public class PaddleController : MonoBehaviour {

	public Rigidbody2D paddle;
	public float paddleVel = 100;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.LeftArrow)) {
			paddle.velocity = new Vector2 (-20, 0);
		} else if (Input.GetKey (KeyCode.RightArrow)) {
			paddle.velocity = new Vector2 (20, 0);
		} else {
			paddle.velocity = new Vector2 (0, 0);
		}
	}
}
